#include "Entity.h"
#include <string.h>
#include <assert.h>

Entity *newEntity(int x, int y, int w, int h) {
    Entity* p = (Entity*)malloc(sizeof(Entity));
    if (!p) {
        mPrint("Failed to malloc a new player, exiting\n");
        exit(-1);
    }
    p->x=x;
    p->y=y;
    p->xv = 0;
    p->yv = 0;
    p->x_dest = x;
    p->y_dest = y;
    p->currentClip=(SDL_Rect *)malloc(sizeof(SDL_Rect));
    if (p->currentClip == NULL) {
        mPrint("Failed to allocate SDL_Rect, exiting");
        exit(-1);
    }
    p->clipIndex = 0;
    p->numClips = 1;
    p->animContext = 0;
    p->animContextCount = 1;
    p->currentClip->x = 0;
    p->currentClip->y = 0;
    p->currentClip->w = w;
    p->currentClip->h = h;
    p->spritesheet = NULL;
    p->currentSpritesheetRect = NULL;
    p->isFlipped = false;
    p->isAnimating = false;
    p->angle = 0;
    
    p->next = NULL;
    p->prev = NULL;

    p->isPlayer = false;
    p->isEnemy = false;
    p->isInnertube = false;
    p->isVisible = true;
    p->isDestroyed = false;

    p->entityType = EntityType_None; 

    return p;
}


void Entity_free(Entity *p) {
    if (!p) return;
    assert(p);
    mPrint("Entity_free");
    free(p->currentClip);
    free(p->spritesheet);
    free(p->currentSpritesheetRect);
    free(p);
    mPrint("End of Entity_free");
}


int Entity_loadSpritesheet(Entity *p, char *filename, int numClips, SDL_Renderer *r) {
    
    assert(p);
    assert(filename);
    assert( strcmp(filename, "") != 0 );
    assert(r);
    //if (!p || !filename || strcmp(filename,"")==0) {
    //    mPrint("No player struct or filename provided, returning!");
    //    return -1;
    //}

    //if (r==NULL) {
    //    mPrint("No renderer provided, returning!");
    //    return -1;
    //}


    p->spritesheet = LTexture_new();
    
    LTexture_setRenderer(p->spritesheet, r);
    if (! LTexture_loadFromFile(p->spritesheet, filename)) {
        mPrint("Failed to load LTexture from file");
        return -1;
    }

    p->currentSpritesheetRect = (SDL_Rect *)malloc(sizeof(SDL_Rect));
    
    if (p->currentSpritesheetRect == NULL) {
        mPrint("Failed to allocate currentSpritesheetRect, exiting");
        return -1;
    }

    p->numClips = numClips;
    
    return 0;
}


int Entity_draw(Entity *p, double scale, double angle, SDL_Point *center, SDL_RendererFlip flip) {
    
    assert(p);

    if (p->isVisible == false) {
        return 0;
    }

    if (p->isDestroyed) {
        return 0;
    }

    double x = p->x;
    double y = p->y;

    x -= p->spritesheet->mHeight;
    y -= p->spritesheet->mHeight;

    LTexture_render(p->spritesheet, 
            (int)x, 
            (int)y, 
            scale, 
            p->currentClip, 
            angle, 
            center,
            flip
    );    


    
    return 0;
}



void Entity_setAnimContext(Entity* p, int context) {
    assert(p);
    //if (!p) return;
    p->animContext = context;
}


void Entity_updateClip(Entity *p) {
    assert(p);
    //if (p == NULL) {
    //    mPrint("p is NULL");
    //    exit(-1);
    //}
    if (p->numClips > 1 && p->isAnimating) {
        p->clipIndex++;
        if (p->clipIndex >= p->numClips) {
            p->clipIndex = 0;
        }
        p->currentClip->x = (p->currentClip->w) * (p->clipIndex);
    }
}


void Entity_flipAnimationOnOff(Entity* p) {
    assert(p);
    p->isAnimating = !p->isAnimating;
    // experiment - looks nice
    p->clipIndex = 0;
}

