#pragma once
#include "SDL_handler.h"
#include <stdbool.h>
typedef struct {
    bool mPaused;
    bool mStarted;
    Uint32 mStartTicks;
    Uint32 mPausedTicks;
} LTimer;
LTimer *LTimer_new();
void LTimer_start(LTimer *t);
void LTimer_stop(LTimer *t);
void LTimer_pause(LTimer *t);
void LTimer_unpause(LTimer *t);
Uint32 LTimer_getTicks(LTimer *t);
bool LTimer_isStarted(LTimer *t);
bool LTimer_isPaused(LTimer *t);
