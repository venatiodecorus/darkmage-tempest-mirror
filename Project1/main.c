#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>
#include "SDL_handler.h"
#include "LTexture.h"
#include "mPrint.h"
#include "LTimer.h"
#include "Entity.h"
#include "Gamestate.h"
#include "Render.h"
#include "LBitmapFont.h"

#include <math.h>

#include <SDL_mixer.h>


bool graphicsInit( Gamestate *gs );
bool loadMedia( Gamestate *gs );
void doFPSCalc(Gamestate *gs, LTimer *t);
void close();
void debugPrintKeyDown(int keycode);
void debugPrintKeyUp(int keycode);
void handleMouseMotion(Gamestate *gs, int xrel, int yrel);
void handleInput(Gamestate *gs, SDL_Event *e);
void handleKeydown(Gamestate *gamestate, SDL_Event *e);
void handleKeyup(Gamestate *gamestate, SDL_Event *e);
void initAll( Gamestate **gs );
void handlePlayerShotFired( Gamestate *gs );
void initTargetTexture(Gamestate *gs);
void initPlayer( Gamestate *gs );
void initTitleScreen( Gamestate *gs ) ;
void initEnemyList( Gamestate *gs ) ;
void addEnemy( Gamestate *gs ) ;
//void addBullet( Gamestate *gs ) ;
void initDebugPanel(Gamestate *gs);
void setDebugPanelText(Gamestate *gs);
void handleKeyboardState(Gamestate *gamestate) ;
void toggleFullscreen() ;

// Config variables
bool debugPanelIsLoaded    = false;
bool debugPanelTextChanged = false;
bool quit                  = false;
bool mFullScreen           = false;
bool mMinimized            = false;
bool spacebarIsPressed     = false;
char startTimeBuf[64]     = {0};
char currentTimeBuf[64]   = {0};
int prevKeycode = 0;

//LTexture* stonetile        = NULL;

SDL_Keycode currentKeyPressed = 0;
SDL_Surface  *gScreenSurface  = NULL;
SDL_Window   *gWindow         = NULL;
time_t startTime;
time_t currentTime;
TTF_Font *gDebugPanelFont = NULL;

LTexture *testFontTexture = NULL;

LBitmapFont *testFont = NULL;


bool isFullscreen = false;










int main( int argc, char* args[] ) {
    LTimer *fpsTimer = LTimer_new();
    Gamestate *gs = NULL;
    initAll( &gs );
    SDL_Event e;
    LTimer_start(fpsTimer);
    while (!quit) {
        doFPSCalc(gs, fpsTimer);
        handleInput( gs, &e );
        updateState( gs );
        

        //int onscreenBulletCount = 10;
        

        if ( gs->enemyCount == 0 ) {
            
            for(int i=0; i<5; i++) {
                addEnemy( gs );
            }
        }



        renderFrame( gs );
        //LBitmapFont_renderText( testFont, 10, 10, "Hello" );
        setDebugPanelText( gs );
    }
    close();
    return 0;
}

void initAll( Gamestate **gs ) {
    srand((unsigned int) time(NULL));
    assert(gs != NULL);

    *gs = Gamestate_new(0, 0, 1.0);

    Gamestate_setCircleRadius(*gs, 100);

    (*gs)->innertubeRect = (SDL_Rect *) calloc( 1, sizeof(SDL_Rect) );
    if ((*gs)->innertubeRect == NULL) {
        perror("Error mallocing");
        exit(-1);
    }
    int x = (*gs)->DefaultScreenWidth/2;
    int y = (*gs)->DefaultScreenHeight/2;
    int w = 25;
    int h = 25;
    x -= (w/2);
    y -= (h/2);
    (*gs)->innertubeRect->x = x;
    (*gs)->innertubeRect->y = y;
    (*gs)->innertubeRect->w = w;
    (*gs)->innertubeRect->h = h;

    (*gs) -> moveMode = MOVEMODE_PLAYER;
    
    bool success = graphicsInit( *gs );
    if (!success) {
        mPrint("Failed to graphicsInit()");
        exit(-1);
    }


    // init audio
    if ( Mix_OpenAudio ( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 ) {
        printf("Failed to init SDL_Mixer: %s\n", Mix_GetError());
        exit(-1);
    }




    initPlayer( *gs );
    initTitleScreen( *gs );
    success = loadMedia( *gs );
    
    if (!success) { 
        mPrint("Failed to load media");
        exit(-1);
    }
    
    initDebugPanel( *gs ); 
    startTime = time(NULL);    
    debugPanelTextChanged = true;
    setDebugPanelText( *gs );

    initEnemyList( *gs );

    (*gs)->currentScene = GamestateSceneTitle;

    

    if ( Mix_PlayingMusic() == 0 ) {
        
        Mix_PlayMusic( (*gs)->backgroundMusic, -1 );
    }




}


bool graphicsInit( Gamestate *gs ) {
    mPrint("graphicsInit()");
    if (!gs) {
        mPrint("gs is NULL");
        return false;
    }

    bool success = true;

    mPrint("calling SDL_Init...");

    int initFlags = SDL_INIT_VIDEO | SDL_INIT_AUDIO;
    int result = SDL_Init(initFlags);

    printf("Result: %d\n", result);

    if ( result < 0 ) {
        printf("SDL could not initialize, result < 0: %s\n", SDL_GetError());
        return false;
    } 
    else if (result > 0) {
        printf("SDL could not initialize, result > 0: %s\n", SDL_GetError());
        return false;
    }

    Uint32 result2 = SDL_WasInit(SDL_INIT_VIDEO);
    if (result2) {
        mPrint("Video initialized");
    } 
    else {
        mPrint("Video NOT initialized");

    }

    result2 = SDL_WasInit(SDL_INIT_AUDIO);
    if (result2) {
        mPrint("Audio initialized");
    } 
    else {
        mPrint("Audio NOT initialized");

    }

    mPrint("calling SDL_CreateWindow...");
    gWindow = SDL_CreateWindow("myGame",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gs->DefaultScreenWidth, gs->DefaultScreenHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (!gWindow) {
        printf("Window could not be created!: %s\n", SDL_GetError());
        return false;
    }

    gs->r = SDL_CreateRenderer(gWindow,-1,SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
    
    if (gs->r==NULL) {
        printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
        return false;
    }

    SDL_SetRenderDrawColor(gs->r, 0xff, 0xff, 0xff, 0xff);
    int imgFlags = IMG_INIT_PNG;
    if ( ! (IMG_Init(imgFlags) & imgFlags) ) {
        printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
        return false;
    }

    if ( TTF_Init() == -1 ) {
        printf("SDL_ttf could not init! SDL_ttf Error: %s\n",TTF_GetError());
        return false;
    }

    gScreenSurface = SDL_GetWindowSurface(gWindow);
    if (gScreenSurface == NULL) {
        mPrint("Failed to SDL_GetWindowSurface, exiting");
        return false;
    }

    // targetTexture is supposed to be the 'screen texture' so we can resize it as necessary
    // without having to resize individual entities within it manually (different zoom context)
    initTargetTexture(gs);

    // We'll come back to this...

    //testFontTexture = LTexture_new();
    //LTexture_setRenderer( testFontTexture, gs->r );
    //LTexture_loadFromFile( testFontTexture, "img/font/test.png" );
    //testFont = LBitmapFont_createNew(); 
    //LBitmapFont_buildFont( testFont, testFontTexture );

    mPrint("end of graphicsInit()");
    return true;
}


void initTargetTexture(Gamestate *gs) {
    gs->targetTexture = LTexture_new();
    
    int success = LTexture_create(gs->targetTexture, 
            gs->r, 
            SDL_PIXELFORMAT_RGBA8888, 
            SDL_TEXTUREACCESS_TARGET, 
            gs->DefaultScreenWidth, 
            gs->DefaultScreenHeight 
            );

    if (!success) {
        mPrint("Failed to LTexture_create targetTexture, exiting");
    }
}


bool loadMedia( Gamestate *gs ) {
    if (!gs) return false;
    bool success = true;
    SDL_Color textColor = {255,255,255,255}; // load the debug panel font
    int playerSpriteFrameCount = 1;
    //char* playerSpriteFilePath = "img/pepe-basic-dm-Sheet.png";
    char* playerSpriteFilePath = "img/tempest1.png";
    
    char* stonetileFilePath    = "img/rpg/stonetile.png";
    
    mPrint("loadMedia()");
    
    int fontSize = 14;
    
    gDebugPanelFont = TTF_OpenFont("Terminus.ttf", fontSize);
    
    mPrint("Opening font...");
    mPrint("Setting font style...");
    
    TTF_SetFontStyle(gDebugPanelFont, TTF_STYLE_BOLD );
    
    if ( gDebugPanelFont == NULL ) {
        mPrint("Failed to load font!\n");
        return false; 
    }

    gs->gFPSTexture = LTexture_new();
    LTexture_setFont( gs->gFPSTexture, gDebugPanelFont);
    LTexture_setRenderer( gs->gFPSTexture, gs->r);
    
    if ( ! LTexture_loadFromRenderedText( gs->gFPSTexture, "FPS: ", textColor ) ) {
        mPrint( "Failed to load text texture!\n" );
        return false;
    }
    
    // init stone tile LTexture
    //stonetile = LTexture_new();
    //LTexture_setRenderer(stonetile, gs->r);
    //LTexture_loadFromFile(stonetile, stonetileFilePath);
    
    // init player entity spritesheet
    mPrint("Loading player spritesheet");
    if (Entity_loadSpritesheet( gs->player, playerSpriteFilePath, playerSpriteFrameCount, gs->r ) ) {
        mPrint("Failed to load player spritesheet, exiting");
        exit(-1);
    }
    Entity_flipAnimationOnOff(gs->player);
    mPrint("Player spritesheet loaded");

    // init enemy entity spritesheet
    /*
    char* enemySpriteFilePath = "img/tempest_enemy1.png";
    if (Entity_loadSpritesheet( gs->enemyList, enemySpriteFilePath, playerSpriteFrameCount, gs->r ) ) {
        mPrint("Failed to load enemy spritesheet, exiting");
        exit(-1);
    }
    */

    mPrint("Loading titlescreen");
    char *titlescreenFilePath = "img/title4.png";
    if ( Entity_loadSpritesheet( gs->titleScreenEntity, titlescreenFilePath, 1, gs->r ) ) {
        mPrint("Failed to load titlescreen");
        exit(-1);
    }

    SDL_SetRenderDrawBlendMode(gs->r, SDL_BLENDMODE_BLEND);


    // load audio
    
    char *backgroundMusicFile = "audio/darkflow.mp3";

    gs -> backgroundMusic = Mix_LoadMUS(backgroundMusicFile);
    if (gs -> backgroundMusic == NULL) {
        printf("Failed to load bg music: %s\n", Mix_GetError());
        exit(-1);
    }

    char *splatEffectFile = "audio/splat.wav";
    gs -> splatEffect = Mix_LoadWAV(splatEffectFile);
    if (gs -> splatEffect == NULL) {
        printf("Failed to load splat effect: %s\n", Mix_GetError());
        exit(-1);
    }



    mPrint("end of loadMedia()");
    return success;
}


void doFPSCalc(Gamestate *gs, LTimer *t){ 
    if (!gs) return; 
    gs->avgFPS = gs->frame_count / (LTimer_getTicks(t) / 1000.0f); 
}



#define PLAYER_ANGLE_INCR 0.1

void handleMouseMotion(Gamestate *gs, int xrel, int yrel) {
    assert(gs);
    // mouse going down-right
    if (xrel > 0 && yrel > 0) { gs->playerAngle += PLAYER_ANGLE_INCR; }
    // mouse going right
    else if (xrel > 0 && yrel == 0) { gs->playerAngle += PLAYER_ANGLE_INCR; } 
    // mouse going up-right
    else if (xrel > 0 && yrel < 0) { gs->playerAngle += PLAYER_ANGLE_INCR; }
    // mouse going nowhere
    //else if (xrel == 0 && yrel == 0) {  } 
    // mouse going left
    else if (xrel < 0 && yrel == 0) { 
        gs->playerAngle -= PLAYER_ANGLE_INCR;   
    }

    // mouse going up-left
    else if (xrel < 0 && yrel < 0) { 
        gs->playerAngle -= PLAYER_ANGLE_INCR;   

    } 

    // mouse going down-left
    else if (xrel < 0 && yrel > 0) { 
        gs->playerAngle -= PLAYER_ANGLE_INCR;   
    
    }

    // mouse going up
    else if (xrel == 0 && yrel < 0) { 
    
    }

    // mouse going down
    else if (xrel == 0 && yrel > 0) { 
        
    }
}




void handlePlayerShotFired( Gamestate *gs ) {
    
}


void handleInput(Gamestate *gs, SDL_Event *e) {
    if (e==NULL || gs==NULL) return; 
    while (SDL_PollEvent(e)!=0) {
        //handleKeyboardState(gs);
        switch(e->type) {
            case SDL_QUIT:
                quit=true;
                break;
            case SDL_KEYUP:
                handleKeyup(gs, e);
                break;
            case SDL_KEYDOWN:
                handleKeydown(gs, e);
                break;
            case SDL_MOUSEMOTION:
                // update the global mouse x and y position for display in 
                // debug panel and other usage
                gs->mouse_x = e->motion.x; 
                gs->mouse_y = e->motion.y;
                gs->rel_mouse_x = e->motion.xrel; 
                gs->rel_mouse_y = e->motion.yrel;
                //handleMouseMotion( gs, gs->rel_mouse_x, gs->rel_mouse_y ) ;
                break;

            case SDL_MOUSEBUTTONDOWN:
                // do something like "blast"
                break;

            case SDL_WINDOWEVENT_ENTER: mPrint("Entered window"); break;
            case SDL_WINDOWEVENT_LEAVE: mPrint("Left window"); break;
            case SDL_WINDOWEVENT_FOCUS_GAINED: mPrint("Focus gained on window"); break;
            case SDL_WINDOWEVENT_FOCUS_LOST: mPrint("Focus lost on window"); break;
            case SDL_WINDOWEVENT_MINIMIZED: mPrint("Minimized window"); break;
            case SDL_WINDOWEVENT_MAXIMIZED: mPrint("Maximized window"); break;
            case SDL_WINDOWEVENT_RESTORED: mPrint("Restored window"); break;

            default: ; break;
        }
        
    }
}


#define SCALE_ZOOM_FACTOR 0.05
#define CAMERA_MOVE_FACTOR (8*gamestate->scale >= 8 ? ((int)(8 * gamestate->scale)) : 8 )
void handleKeydown(Gamestate *gamestate, SDL_Event *e) {
    SDL_Keycode keycode = e->key.keysym.sym;
    //checks for left shift, right shift, or capslock
    //Uint16 mod = e->key.keysym.mod;
    //bool isUppercase=(mod==KMOD_LSHIFT||mod==KMOD_RSHIFT||mod == KMOD_CAPS);
    debugPrintKeyDown((int)keycode);
    

    // if titlescreen, advance to main game
    if ( gamestate->currentScene == GamestateSceneTitle ) {
        gamestate->currentScene = GamestateSceneMainGame;
        return;
    }




    int player_movement = 8;
    int tw = gamestate->tileSizeW;
    int th = gamestate->tileSizeH;
    
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    
    switch(keycode) {
        case 'f':
            //toggleFullscreen();

            // fire a shot
            //addBullet( gamestate );

            gamestate->fireIsPressed = true;

            break;
        case 'd':
            gamestate->renderDebugPanelOn = ! gamestate->renderDebugPanelOn; 
            break;
        
        case 'g':
            gamestate->renderGridOn = ! gamestate->renderGridOn;
            break;
        case 'q':
            quit = true; 
            break;
        case 'z':
            gamestate->scale+=SCALE_ZOOM_FACTOR;
            break;
        case 'x': 
            if (gamestate->scale >= SCALE_ZOOM_FACTOR ) { 
                gamestate->scale-=SCALE_ZOOM_FACTOR; 
            }
            break;
        case 'a':
            Entity_flipAnimationOnOff(gamestate->player);
            break;
        case 'm':
            gamestate->moveMode = gamestate->moveMode==MOVEMODE_CAMERA ? MOVEMODE_PLAYER : MOVEMODE_CAMERA; 
            break;
        case ' ':
            gamestate->spacebarIsPressed = true;
            break;
        case SDLK_LEFT: 
            gamestate->leftIsPressed = true;
            break;

        case SDLK_RIGHT:
            gamestate->rightIsPressed = true;
            break;

        case SDLK_UP:
            gamestate->upIsPressed = true;
            break;
        case SDLK_DOWN:
            gamestate->downIsPressed = true;
            break;

        default: ;
    }
    //handleKeyboardState(gamestate);
}


void handleKeyup(Gamestate *gamestate, SDL_Event *e) {
    SDL_Keycode keycode = e->key.keysym.sym;
    //checks for left shift, right shift, or capslock
    //Uint16 mod = e->key.keysym.mod;
    //bool isUppercase=(mod==KMOD_LSHIFT||mod==KMOD_RSHIFT||mod == KMOD_CAPS);
    debugPrintKeyUp((int)keycode);
    //handleKeyboardState(gamestate);
    switch(keycode) {
        case 'f':
            gamestate->fireIsPressed = false;
            break;
        case ' ':
            gamestate->spacebarIsPressed = false;
            break;
        case SDLK_LEFT:
            gamestate->leftIsPressed = false;
            break;
        case SDLK_RIGHT:
            gamestate->rightIsPressed = false;
            break;
        case SDLK_UP:
            gamestate->upIsPressed = false;
            break;
        case SDLK_DOWN:
            gamestate->downIsPressed = false;
            break;
        default: ;
    }
}





void handleKeyboardState(Gamestate *gamestate) {
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    assert(state);
    
    if (state[SDL_SCANCODE_SPACE] && state[SDL_SCANCODE_LEFT] ) { 
        mPrint("Left");
        gamestate->spacebarIsPressed = true;
        if (gamestate->moveMode == MOVEMODE_CAMERA) {
            gamestate->camera_x-=CAMERA_MOVE_FACTOR;
        }
        else if (gamestate->moveMode == MOVEMODE_PLAYER) {
            gamestate->playerAngle -= PLAYER_ANGLE_INCR;
        }
    }
    else if (state[SDL_SCANCODE_SPACE] && state[SDL_SCANCODE_RIGHT] ) { 
        mPrint("Right");
        gamestate->spacebarIsPressed = true;
        if (gamestate->moveMode == MOVEMODE_CAMERA) {
            gamestate->camera_x+=CAMERA_MOVE_FACTOR;
        }
        else if (gamestate->moveMode == MOVEMODE_PLAYER) {
            gamestate->playerAngle += PLAYER_ANGLE_INCR;
        }
    }
    else if (state[SDL_SCANCODE_SPACE]) {
        mPrint("Space");
        gamestate->spacebarIsPressed = true;
    }
    else if ( ! state[SDL_SCANCODE_SPACE] ) {
        gamestate->spacebarIsPressed = false;
    }


    if (state[SDL_SCANCODE_LEFT]) {
        mPrint("Left");
        gamestate->spacebarIsPressed = false;
        if (gamestate->moveMode == MOVEMODE_CAMERA) {
            gamestate->camera_x-=CAMERA_MOVE_FACTOR;
        }
        else if (gamestate->moveMode == MOVEMODE_PLAYER) {
            gamestate->playerAngle -= PLAYER_ANGLE_INCR;
        }
    }


    if (state[SDL_SCANCODE_RIGHT]) {
        mPrint("Right");
        gamestate->spacebarIsPressed = false;
        if (gamestate->moveMode == MOVEMODE_CAMERA) {
            gamestate->camera_x+=CAMERA_MOVE_FACTOR;
        }
        else if (gamestate->moveMode == MOVEMODE_PLAYER) {
            gamestate->playerAngle += PLAYER_ANGLE_INCR;
        }
    }
    
    if (state[SDL_SCANCODE_Q]) {
        quit=true;
    }
}










void setDebugPanelText( Gamestate *g ) {
    if (debugPanelTextChanged) {
        snprintf(g->debugPanelText,1024, 
"Health: 0\n\
Frame: %d\n\
FPS: %0.2f\n\
Scale: %0.2f\nCam: %d,%d\n\
Player: %d,%d\nMoveMode: %d\n\
Angle: %0.2f\n\
Mouse: (%d, %d)\n\
RelMouse: (%d, %d)\n\
Buttons: %d %d %d %d\n\
EnemyCount: %d\n\
BulletCount: %d\n\
EnemiesKilled: %d\n",
        g->frame_count,
        g->avgFPS,
        g->scale, g->camera_x, g->camera_y,
        
        (int)g->player->x, (int)g->player->y, g->moveMode,
        
        g->playerAngle , 
        g->mouse_x, g->mouse_y,
        g->rel_mouse_x, g->rel_mouse_y,
        
        g->spacebarIsPressed ? 1 : 0, 
        g->leftIsPressed ? 1 : 0,
        g->rightIsPressed ? 1 : 0,
        g->fireIsPressed ? 1 : 0,

        g->enemyCount,
        g->bulletCount,

        g->enemiesKilled
        );
    }
}

void debugPrintKeyDown(int keycode) { 
    char tmp[10]={'K','e','y',' ','D','n',':',' ',(char)keycode,0}; 
    mPrint(tmp); 
}


void debugPrintKeyUp(int keycode) { 
    char tmp[10]={'K','e','y',' ','U','p',':',' ',(char)keycode,0}; 
    mPrint(tmp); 
}


void initTitleScreen( Gamestate *gs ) {
    assert(gs);
    int x = 0;
    int y = 0;
    int w = 320*2;
    int h = 180*2;
    // why the fuck do i need these odd padding numbers? /\_-_-_/\ no idea
    int ox = 360;
    int oy = 360;
    x += ox;
    y += oy;
    gs->titleScreenEntity = newEntity( x, y, w, h );
    if (! gs->titleScreenEntity) {
        mPrint("Error, failed to create titlescreen");
        exit(-1);
    }
    gs->titleScreenEntity->isVisible = true;
    gs->titleScreenEntity->entityType = EntityType_Titlescreen;
}


void initPlayer( Gamestate *gs ) {
    assert(gs);
    //if (!gs) return;
    
    int x = gs->DefaultScreenWidth / 2;
    int y = gs->DefaultScreenHeight / 2;
    int ox = gs->tileSizeW;
    int oy = gs->tileSizeH;
    int padding = 20;

    x -= ox;
    y -= oy;
    x -= gs->circleRadius;
    x -= padding;

    gs->player = newEntity(x, y, gs->tileSizeW, gs->tileSizeH) ;
    //gs->player->isFlipped = true;
    gs->player->isPlayer = true;

    gs->player->entityType = EntityType_Player;
    gs->player->isVisible = true;
}


void initEnemyList( Gamestate *gs ) {
    assert(gs);

    // this creates ONE enemy
    // we want to generalize this to 
    // 1. create an enemy if list is empty and make it head of list
    // 2. create an enemy and add to end of list if list is not empty
    //
    // this way, we can add multiple enemies

    // as a test, im adding 100 enemies or so to screen
    int enemycount = 5;
    for(int i=0;i<enemycount;i++) {
        addEnemy( gs );
        //gs->enemyCount++;
    }
}













void addEnemy( Gamestate *gs ) {
    assert(gs);
    
    int x = gs->DefaultScreenWidth / 2;
    int y = gs->DefaultScreenHeight / 2;
    int ox = gs->tileSizeW;
    int oy = gs->tileSizeH;
    int padding = 20;

    Entity *enemy = newEntity( x, y, ox, oy );
    enemy->isEnemy = true;
    enemy->entityType = EntityType_Enemy; 
    enemy->isInnertube = true;
    enemy->isVisible = false;
    
    // load enemy sprite
    //char* enemySpriteFilePath = "img/tempest_enemy1.png";
    
    int enemySpriteFileCounts = 3;
    char* enemySpriteFilePaths[3] = { 
        "img/tempest_enemy1.png",
        "img/fetus1.png",
        "img/fetus2.png"
    };


    int randomIndex = rand() % enemySpriteFileCounts;
    char *enemySpriteFilePath = enemySpriteFilePaths[ randomIndex ];


    int enemySpriteFrameCount = 1;
    mPrint("Loading enemy spritesheet");
    if (Entity_loadSpritesheet( enemy, enemySpriteFilePath, enemySpriteFrameCount, gs->r ) ) {
        mPrint("Failed to load enemy spritesheet, exiting");
        exit(-1);
    }

    if (gs->enemyList == NULL) {
        gs->enemyList = enemy;
    }
    else {
        // find end of list
        Entity *currentEntity = gs->enemyList; 

        while (currentEntity->next != NULL) {
            currentEntity = currentEntity->next;
        }

        currentEntity->next = enemy;
        enemy->prev = currentEntity; 
    }

    gs->enemyCount++;
}



void initDebugPanel(Gamestate *gs) {
    if (!gs) return;
    gs->debugPanelRect = (SDL_Rect *)malloc(sizeof(SDL_Rect));
    if (!gs->debugPanelRect) {
        mPrint("Failed to malloc debugPanelRect, exiting");
        exit(-1);
    }
    gs->debugPanelRect->x = gs->debugPanelX;
    gs->debugPanelRect->y = gs->debugPanelY;
    gs->debugPanelRect->w = gs->debugPanelWidth;
    gs->debugPanelRect->h = gs->debugPanelHeight;
}


void close( Gamestate *gamestate ) {
    SDL_DestroyWindow(gWindow);
    Gamestate_free( gamestate );
    gWindow = NULL;
    TTF_CloseFont( gDebugPanelFont );
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}


/*void handleKeyup(SDL_Event *e) {}*/


void toggleFullscreen() {

    if (isFullscreen) {
        SDL_SetWindowFullscreen( gWindow, SDL_FALSE );
    }
    else {
        SDL_SetWindowFullscreen( gWindow, SDL_TRUE );
    }

}




