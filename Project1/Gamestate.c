#include "Gamestate.h"
#include <assert.h>
#include <math.h>


Gamestate *Gamestate_new(int cx, int cy, double sc) {
    Gamestate *gs = (Gamestate *) malloc( sizeof(Gamestate) );
    if (gs==NULL) {
        mPrint("Error: Failed to calloc Gamestate, exiting");
        exit(-1);
    }
    gs->DefaultScreenWidth  = 640*1;
    gs->DefaultScreenHeight = 360*1;
    gs->camera_x = cx;
    gs->camera_y = cy;
    gs->tileSizeW = TILE_SIZE_W_DEFAULT; 
    gs->tileSizeH = TILE_SIZE_H_DEFAULT; 
    gs->scale = sc;
    gs->targetTexture = NULL;
    gs->r = NULL;
    gs->player = NULL;
    gs->debugPanelRect = NULL;
    gs->innertubeRect = NULL;
    gs->gFPSTexture = NULL;
    gs->debugPanelX = 5;
    gs->debugPanelY = 5;
    gs->debugPanelWidth  = 200;
    gs->debugPanelHeight = 200;
    gs->renderDebugPanelOn = false;
    gs->renderGridOn = false;
    gs->frame_count = 0;
    gs->avgFPS = 0;
    gs->circleRadius = 0;
    gs->playerAngle = 0;
    gs->mouse_x = 0;
    gs->mouse_y = 0;
    gs->rel_mouse_x = 0;
    gs->rel_mouse_y = 0;
    gs->spacebarIsPressed = false;
    gs->leftIsPressed = false;
    gs->rightIsPressed = false;
    gs->fireIsPressed = false;
    memset( gs->debugPanelText, 0, 1024 );

    gs->enemyList = NULL;
    gs->bulletList = NULL;
    gs->enemyCount = 0;
    gs->bulletCount = 0;

    gs->enemiesKilled = 0;

    gs->currentScene = GamestateSceneNone;

    gs->titleScreenEntity = NULL;

    gs->backgroundMusic = NULL;
    gs->splatEffect = NULL;

    gs->moveMode = MOVEMODE_PLAYER;

    gs->upIsPressed=false;
    gs->downIsPressed=false;
    gs->leftIsPressed=false;
    gs->rightIsPressed=false;
    gs->fireIsPressed=false;
    gs->spacebarIsPressed=false;

    return gs;
}





// got a LOT to add here, lol...-_-;;;
void Gamestate_free( Gamestate *gs ) {
    if (!gs) return;
    Entity_free(gs->player);
    SDL_DestroyRenderer(gs->r);
    
    LTexture_free( gs->targetTexture );
    
    LTexture_free( gs->gFPSTexture );
    
    free(gs->debugPanelRect);
    
    gs->targetTexture = NULL;
    gs->r = NULL;
    gs->player = NULL;
    gs->debugPanelRect = NULL;
    gs->gFPSTexture = NULL;
}


void handleEntityMove( Gamestate *g, Entity *p ) {
    assert(p!=NULL);
    assert(g!=NULL);
    double padding = 20;
    double offset = g->tileSizeW;
    double r = g->circleRadius + padding + offset;
    p->x = (int)(r * cos( g->playerAngle ) + g->DefaultScreenWidth / 2); 
    p->y = (int)(r * sin( g->playerAngle ) + g->DefaultScreenHeight / 2);
}


void handlePlayerMove( Gamestate *g, Entity *p ) {
    assert(p!=NULL);
    assert(g!=NULL);
    double padding = 20;
    double offset = g->tileSizeW;
    double r = g->circleRadius + padding + offset;
    p->x = (int)(r * cos( g->playerAngle ) + g->DefaultScreenWidth / 2); 
    p->y = (int)(r * sin( g->playerAngle ) + g->DefaultScreenHeight / 2);
}



void handleEnemyMove( Gamestate *g ) {
    // handle enemy move
    if (g->frame_count % 4 == 0) {
        Entity *currentEntity = g->enemyList;
        while (currentEntity != NULL) {


            if ( currentEntity->isInnertube ) {
                int r = rand() % 3;
                if (r==1)
                    currentEntity->x++;
                else if (r==0)
                    currentEntity->x--;
                r = rand() % 3;
                if (r==1)
                    currentEntity->y++;
                else if (r==0)
                    currentEntity->y--;
            }
            else {
                if (g->frame_count % 4 == 0) {
                    int r = rand() % 3;
                    if (r==1)
                        currentEntity->x++;
                    else if (r==0)
                        currentEntity->x--;
                    r = rand() % 3;
                    if (r==1)
                        currentEntity->y++;
                    else if (r==0)
                        currentEntity->y--;
                }
            }

            double x = currentEntity->x;
            double y = currentEntity->y;
            
            if ( ! pixelCollides( (int)x, (int)y, g->innertubeRect ) ) {
                currentEntity->isInnertube = false;
                currentEntity->isVisible = true;
            }

            currentEntity = currentEntity -> next;
        }
    }
}


void handleBulletMove( Gamestate *gamestate ) {
    // handle bullet move
    //mPrint("handleBulletMove");
    //Entity *currentEntity = gamestate->bulletList;
    Entity *knife = gamestate->bulletList;
    
    while (knife != NULL) {
                
        knife->x += knife->xv;
        knife->y += knife->yv;

        bool knifeIsOffscreen = (knife->x < 0 || knife->y < 0 || knife->x > gamestate->DefaultScreenWidth || knife->y > gamestate->DefaultScreenHeight ) ;

        // handle knives that fly off-screen
        if ( knifeIsOffscreen ) {
            // entity is off-screen and needs to be removed
            Entity *tmp = knife; 
            Entity *tmpPrev = tmp->prev;
            //Entity *tmpNext = tmp->next;
            knife = knife -> next;

            // end of list
            if (knife == NULL) {
                //mPrint("End of list");   
                if (tmpPrev!=NULL) {
                    tmpPrev->next = NULL;
                }
            }
            else {
                //mPrint("Inner list");   
                knife->prev = tmpPrev;

                if (tmpPrev != NULL) {
                    tmpPrev->next = knife; 
                }
            }

            tmp->next = NULL;
            tmp->prev = NULL;
            gamestate->bulletCount--;
            if (gamestate->bulletCount == 0) {
                gamestate->bulletList = NULL;
            }
            else if (gamestate->bulletList == tmp) {
                gamestate->bulletList = knife;
            }
            Entity_free( tmp );
            tmp = NULL;
        }
        else {
            
            // check each enemy for collision
            Entity *currentEnemy = gamestate->enemyList;
            
            int kx = (int) knife -> x;
            int ky = (int) knife -> y;
            
            //kx += knife->currentClip->w;
            //ky += knife->currentClip->h;

            SDL_Rect rect = { -1, -1, -1, -1 };
        
            while(currentEnemy != NULL) {
 
                if (currentEnemy->isInnertube != true) {

                    int x = (int)currentEnemy->x;
                    int y = (int)currentEnemy->y;
                    int w = currentEnemy->currentClip->w;
                    int h = currentEnemy->currentClip->h;
                    
                    rect.x = x;
                    rect.y = y;
                    rect.w = w;
                    rect.h = h;

                    bool result = pixelCollides( kx, ky , &rect );

                    if (result) {
                        mPrint("Collision!");
                        currentEnemy -> isVisible = false;
                        currentEnemy -> isDestroyed = true;

                        gamestate->enemiesKilled++;
                    }
                }
                currentEnemy = currentEnemy -> next;
            }
        }

        //mPrint("Next knife");
        if (knife != NULL) {
            knife = knife -> next;
        }
    }
    //mPrint("End of handleBulletMove");
}







void updateEnemyList(Gamestate *gamestate) {
    assert(gamestate);

    Entity *currentEnemy = gamestate->enemyList;
    while (currentEnemy != NULL) {
        if (currentEnemy->isDestroyed) {

            // play sound effect
            Mix_PlayChannel( -1, gamestate->splatEffect, 0 );

            Entity *prevEnemy = currentEnemy->prev;
            Entity *nextEnemy = currentEnemy->next;
            Entity *tmp = currentEnemy; 

            if (prevEnemy != NULL) 
                prevEnemy -> next = nextEnemy; 
            if (nextEnemy != NULL) 
                nextEnemy -> prev = prevEnemy;

            if ( gamestate->enemyList == currentEnemy ) {
                gamestate->enemyList = nextEnemy;
            }

            Entity_free( tmp );
            gamestate->enemyCount--;

            if (gamestate->enemyCount == 0) {
                gamestate->enemyList = NULL;
            }
            
            currentEnemy = nextEnemy;
            //currentEnemy = gamestate->enemyList;

            if (currentEnemy == NULL) {
                break;
            }
        }
        else {
            currentEnemy = currentEnemy -> next;
        }
    }
}







#define PLAYER_ANGLE_INCR 0.1
void updateState(Gamestate *gamestate) {
    assert(gamestate);
    
    // handle player move
    handlePlayerMove( gamestate, gamestate->player );
    handleEnemyMove( gamestate );
    handleBulletMove ( gamestate );
    updateEnemyList( gamestate );

    if ( gamestate->moveMode == MOVEMODE_PLAYER ) {
        if (gamestate->leftIsPressed) {
            gamestate->playerAngle -= PLAYER_ANGLE_INCR;
        }
        if (gamestate->rightIsPressed) { 
            gamestate->playerAngle += PLAYER_ANGLE_INCR;
        }
        if ( gamestate->fireIsPressed ) {
            addBullet( gamestate );
        }
    }

    else if ( gamestate -> moveMode == MOVEMODE_CAMERA ) {
        if (gamestate->leftIsPressed) {
            gamestate->camera_x--;
        }
        
        if (gamestate->rightIsPressed) { 
            gamestate->camera_x++;
        }


        if (gamestate->upIsPressed) { 
            gamestate->camera_y--;
        }
        
        if (gamestate->downIsPressed) { 
            gamestate->camera_y++;
        }
    }


    

    //mPrint("End of updateState");
}


bool pixelCollides(int x, int y, SDL_Rect *rect) {
    assert(rect);

    int x1 = rect->x;
    int y1 = rect->y;

    int x2 = x1 + rect->w;
    int y2 = y1 + rect->h;

    // four corners:
    // (x1, y1)          (x2, y1)
    // (x1, y2)          (x2, y2)

    bool check1 = x >= x1 && y >= y1;
    bool check2 = x <= x2 && y <= y2;

    if (check1 && check2) {
        return true;
    }

    return false;
}





void Gamestate_setCircleRadius(Gamestate *gs, int r) {
    assert(gs!=NULL);
    assert(r>0);
    gs->circleRadius = r;
}


void Gamestate_setPlayerAngle(Gamestate *gs, double angle) {
    assert(gs!=NULL);
    gs->playerAngle = angle;
}



void addBullet( Gamestate *gs ) {
    assert(gs);

    //double x = gs->player->x;
    //double y = gs->player->y;
    double x0 = gs->DefaultScreenWidth/2;
    double y0 = gs->DefaultScreenHeight/2;
    
    int ox = 12;
    int oy = 6;
    
    double x = gs->player->x + (gs->tileSizeW/2);
    double y = gs->player->y + (gs->tileSizeH/2);
    int padding = 20;

    Entity *bullet = newEntity( (int)x, (int)y, ox, oy );
    bullet->entityType = EntityType_Bullet; 
    bullet->isVisible = true;
    
    // the bullet direction needs to be calculated based on the direction we draw the line from the center to the skull
    //ox = gs->tileSizeW;
    //oy = gs->tileSizeH;
   
    // distance formula

    //x += (gs->tileSizeW/2);
    //y += (gs->tileSizeH/2);

    double c = distance( x0, y0, x, y );
    double b = fabs( x - x0 );
    double a_squared = pow(c, 2) - pow(b, 2);
    double a = sqrt( a_squared );
    double xv = 0;
    double yv = 0;
    xv = 0.5 * b / a;
    yv = 1 * a / a;
    if ( x > x0 ) {
        xv = -xv;
    }
    if ( y > y0 ) {
        yv = -yv;
    }
    bullet->xv = xv;
    bullet->yv = yv;
    printf("Bullet Velocity: (%0.2f, %0.2f)\n", 
            xv, yv);


    char* bulletSpriteFilePath = "img/knife3.png";
    int bulletSpriteFrameCount = 1;
    mPrint("Loading bullet spritesheet");
    if (Entity_loadSpritesheet( bullet, bulletSpriteFilePath, bulletSpriteFrameCount, gs->r ) ) {
        mPrint("Failed to load bullet spritesheet, exiting");
        exit(-1);
    }

    if (gs->bulletList == NULL) {
        gs->bulletList = bullet;
    }
    else {
        // find end of list
        Entity *currentEntity = gs->bulletList; 

        while (currentEntity->next != NULL) {
            currentEntity = currentEntity->next;
        }

        currentEntity->next = bullet;
        bullet->prev = currentEntity;
    }

    gs->bulletCount++;
}


double distance(double x0, double y0, double x1, double y1) {
    double t0 = pow( x1 - x0, 2 );
    double t1 = pow( y1 - y0, 2 );
    double t2 = t0 + t1;
    double d = sqrt( t2 );
    return fabs(d);
}

