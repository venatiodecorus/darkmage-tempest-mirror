
//#pragma once

#ifndef __POLYGON_H
#define __POLYGON_H

#include "SDL_handler.h"
#include "Gamestate.h"

typedef struct {
    unsigned int sides;
    SDL_Color color;
    SDL_Point *points;
    
    //Gamestate *gs;

} Polygon;

Polygon *Polygon_new();
Polygon *Polygon_new_with_sides(int s);

void Polygon_free(Polygon *p);
void Polygon_setPointXY(Polygon *p, unsigned int index, int x, int y);

SDL_Point * Polygon_getPointXY(Polygon *p, unsigned int index);

//void Polygon_setGamestate(Polygon *p, Gamestate *gs );
//void Polygon_draw( Polygon *p );

void Polygon_setColorRGBA( Polygon *p, int r, int g, int b, int a );

#endif

