#include "LTexture.h"
#include "mPrint.h"
#include <stdio.h>
#include <string.h>

LTexture *LTexture_new() {
    LTexture *t = (LTexture *)malloc(sizeof(LTexture));
    if (!t) {
        mPrint("Failed to malloc LTexture.\n");
        return NULL;
    }
    t->mTexture=NULL;
    t->gRenderer=NULL;
    t->gFont=NULL;
    t->mWidth=0;
    t->mHeight=0;
    return t;
}

bool LTexture_create(LTexture *t, SDL_Renderer *r, Uint32 format, int access, int w, int h) {
    mPrint("LTexture_create");
    if (t==NULL) {
        mPrint("Error LTexture_create: t is NULL");
        return false; 
    }
    if (r==NULL) {
        mPrint("Error LTexture_create: r is NULL");
        return false;
    }
    SDL_Texture *newTexture = SDL_CreateTexture(r, format, access, w, h);
    if (newTexture == NULL) {
        mPrint("Error creating new texture");
        return false;
    }
    t->mTexture = newTexture;
    LTexture_setRenderer(t, r);
    t->mWidth = w;
    t->mHeight = h;
    return t->mTexture != NULL;
}

bool LTexture_loadFromFile( LTexture *t, char * path ) {
    mPrint("LTexture_loadFromFile");
    
    if (!t) {
        mPrint("Error LTexture_loadFromFile: t is NULL\n");
        return false;
    }
    else if (!path || strcmp(path,"")==0) {
        mPrint("Error LTexture_loadFromFile: path is NULL or empty\n");
        return false;
    }

    //Get rid of preexisting texture
    LTexture_free(t);

     //The final texture
    SDL_Texture* newTexture = NULL;

    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load( path );

    if( loadedSurface == NULL ) {
        printf( "Unable to load image %s! SDL_image Error: %s\n", path, IMG_GetError() );
    }
    else {


        SDL_Surface *formattedSurface = SDL_ConvertSurfaceFormat( loadedSurface, SDL_PIXELFORMAT_RGBA8888, 0 );
        if (formattedSurface == NULL) {
            mPrint("Unable to create formatted surface !");
            printf("SDL Error: %s\n", SDL_GetError() );
            return false;
        }
        
        newTexture = SDL_CreateTexture( t->gRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h );
        if (newTexture==NULL) {
            mPrint("Unable to create blank texture!");
            printf("SDL Error: %s\n", SDL_GetError() );
            return false;
        }

        SDL_SetTextureBlendMode( newTexture, SDL_BLENDMODE_BLEND );
        SDL_LockTexture( newTexture, &formattedSurface->clip_rect, &t->mPixels, &t->mPitch );
        memcpy( t->mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h );

        t->mWidth = formattedSurface->w;
        t->mHeight = formattedSurface->h;

        Uint32* pixels = (Uint32*) t->mPixels; 
        int pixelCount = (t->mPitch / 4) * t->mHeight;

        Uint32 colorKey = SDL_MapRGB( formattedSurface->format,     0x00, 0xFF, 0xFF );
        Uint32 transparent = SDL_MapRGBA( formattedSurface->format, 0x00, 0xFF, 0xFF, 0x00 );

        // color key pixels
        for( int i = 0; i < pixelCount; i++ ) {
            if ( pixels[i] == colorKey ) {
                pixels[i] = transparent;
            }
        }

        SDL_UnlockTexture( newTexture );
        t->mPixels = NULL;

        SDL_FreeSurface( formattedSurface );
        SDL_FreeSurface( loadedSurface );
    }

    
    t->mTexture = newTexture;
    return t->mTexture != NULL;
}

/*

        //Color key image
        SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );
        //Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( t->gRenderer, loadedSurface );
        if( newTexture == NULL ) {
            printf( "Unable to create texture from %s! SDL Error: %s\n", path, SDL_GetError() );
        }
        else {
            //Get image dimensions
            t->mWidth = loadedSurface->w;
            t->mHeight = loadedSurface->h;
        }
        //Get rid of old loaded surface
        SDL_FreeSurface( loadedSurface );
    }
    //Return success
    t->mTexture = newTexture;
    return t->mTexture != NULL;
*/

bool LTexture_loadFromRenderedText( LTexture *t, char * textureText, SDL_Color textColor ) {
    //Get rid of preexisting texture
    if (!t) {
        mPrint("Error LTexture_loadFromRenderedText: t is NULL\n");
        return false;
    }
    LTexture_free(t);
    //Render text surface
#define WRAP_LEN 2048
    SDL_Surface* textSurface = TTF_RenderText_Blended_Wrapped( t->gFont, textureText, textColor, WRAP_LEN );
    if( textSurface == NULL ) {
        printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
    }
    else {
        //Create texture from surface pixels
        t->mTexture = SDL_CreateTextureFromSurface( t->gRenderer, textSurface );
        if( t->mTexture == NULL ) {
            printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
        }
        else {
            //Get image dimensions
            t->mWidth = textSurface->w;
            t->mHeight = textSurface->h;
        }
        //Get rid of old surface
        SDL_FreeSurface( textSurface );
    }
    //Return success
    return t->mTexture != NULL;
}

void LTexture_free(LTexture *t) {
    //mPrint("LTexture_free");
    //Free texture if it exists
    if (t==NULL) {
        mPrint("t is NULL, returning");
        return;
    }
    else if( t->mTexture != NULL ) {
        //mPrint("t has a non-NULL mTexture, freeing");
        SDL_DestroyTexture( t->mTexture );
        t->mTexture = NULL;
        t->mWidth = 0;
        t->mHeight = 0;
    }
}

void LTexture_render( LTexture *t, int x, int y, double scale, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip ) {
    if (t==NULL) {
        mPrint("t is NULL");
        return;
    }
    //Set rendering space and render to screen
    SDL_Rect renderQuad = { x, y, (int)(scale * t->mWidth), (int)(scale * t->mHeight) };
    //Set clip rendering dimensions
    if( clip != NULL ) {
        renderQuad.w = (int)(scale * clip->w);
        renderQuad.h = (int)(scale * clip->h);
    }
    //Render to screen
    int success = SDL_RenderCopyEx( t->gRenderer, t->mTexture, clip, &renderQuad, angle, center, flip );
    if (success != 0) {
        mPrint("Error LTexture_render: failed to SDL_RenderCopyEx");
        if (t->gRenderer==NULL) {
            mPrint("Forgot to set renderer");
        }
        exit(-1);
    }
}

void LTexture_setColor(LTexture *t,Uint8 r,Uint8 g,Uint8 b){if(!t)return;SDL_SetTextureColorMod(t->mTexture,r,g,b);}
void LTexture_setBlendMode(LTexture *t,SDL_BlendMode blending){if(!t)return;SDL_SetTextureBlendMode(t->mTexture,blending);}
void LTexture_setAlpha(LTexture *t,Uint8 alpha){if(!t)return;SDL_SetTextureAlphaMod(t->mTexture,alpha);}
int LTexture_getWidth(LTexture *t){return !t?-1:t->mWidth;}
int LTexture_getHeight(LTexture *t){return !t?-1:t->mHeight;}
void LTexture_setRenderer(LTexture *t,SDL_Renderer *r){if(!t)return;t->gRenderer=r;}
void LTexture_setFont(LTexture *t, TTF_Font *font) {if (!t) return; t->gFont = font;}
SDL_Renderer *LTexture_getRenderer(LTexture *t) {return !t ? NULL : t->gRenderer;}
TTF_Font *LTexture_getFont(LTexture *t) {return !t ? NULL : t->gFont;}

void LTexture_setAsRenderTarget(LTexture *t){
    if(t==NULL){
        mPrint("LTexture_setAsRenderTarget error: t==NULL");exit(-1);}
    if(t->gRenderer==NULL){
        mPrint("Error: t->gRenderer==NULL");exit(-1);}
    if(t->mTexture==NULL){
        mPrint("Error: t->mTexture==NULL");exit(-1);}
    int success=SDL_SetRenderTarget(t->gRenderer, t->mTexture);
    if(success!=0){
        mPrint("Error");
        printf("More: %s\n", SDL_GetError());
        exit(-1);
    }
}

Uint32 LTexture_getPixel32( LTexture *t, unsigned int x, unsigned int y ) {
    mPrint("yo");
    printf("x: %u\n", x);
    printf("y: %u\n", y);

    if (!t) return -1;
    
    Uint32 *pixels = (Uint32 *) t->mPixels; 
    
    unsigned int loc = (y * (t->mPitch / 4)) + x;

    printf("loc: %u\n", loc);

    Uint32 pixel = pixels[loc];

    printf("pixel: %u\n", pixel);

    return pixel;
}

bool LTexture_lockTexture(LTexture *t) {
    if (!t) return false;
    if (t->mPixels != NULL) {
        return false;
    }
    else if ( SDL_LockTexture( t->mTexture, NULL, &t->mPixels, &t->mPitch ) != 0 ) {
        mPrint("Unable to lock texture!");
        return false;
    }
    return true;
}

bool LTexture_unlockTexture(LTexture *t) {
    
    if (!t) return false;

    if (t->mPixels == NULL) {
        return false;
    }

    SDL_UnlockTexture( t->mTexture );
    t->mPixels = NULL;
    t->mPitch = 0;

    return true;
}

void * LTexture_getPixels(LTexture *t) {
    if (!t) return NULL;

    return t->mPixels;
}

int LTexture_getPitch(LTexture *t) { 
    if (!t) return -1;

    return t->mPitch;
}

