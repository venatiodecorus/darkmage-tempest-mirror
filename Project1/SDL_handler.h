#pragma once
// Windows
#ifdef _WIN64
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#else
// MacOS
#ifdef __APPLE__
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL.h>
// Windows 32-bit
#else
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif
#endif
