#include "LTimer.h"
#include <stdio.h>

LTimer *LTimer_new() {
    LTimer *t = (LTimer *) malloc(sizeof(bool)*2);
    if (!t) {
        printf("Error mallocing LTimer, exiting\n");
        exit(-1);
    }
    t->mPaused  = false;
    t->mStarted = false;
    t->mStartTicks = 0;
    t->mPausedTicks = 0;
    return t;
}

void LTimer_start(LTimer *t) {
    if (!t) return;
    t->mStarted = true;
    t->mPaused  = false;
    t->mStartTicks  = SDL_GetTicks();
    t->mPausedTicks = 0;
}

void LTimer_stop(LTimer *t) {
    if (!t) return;
    t->mStarted = false;
    t->mPaused = false;
    t->mStartTicks = 0;
    t->mPausedTicks = 0;
}

void LTimer_pause(LTimer *t) {
    if (!t) return;
    if (t->mStarted && !t->mPaused) {
        t->mPaused = true;
        t->mPausedTicks = SDL_GetTicks() - (t->mStartTicks);
        t->mStartTicks = 0;
    }
}

void LTimer_unpause(LTimer *t) {
    if (!t) return;
    if (t->mStarted && t->mPaused) {
        t->mPaused = false;
        t->mStartTicks = SDL_GetTicks() - (t->mPausedTicks);
        t->mPausedTicks = 0;
    }
}

Uint32 LTimer_getTicks(LTimer *t) {
    if (!t) return -1;
    Uint32 time = 0;
    if (t->mStarted) {
        if (t->mPaused) {
            time = t->mPausedTicks;
        }
        else {
            time = SDL_GetTicks() - (t->mStartTicks);
        }
    }
    return time;
}

bool LTimer_isStarted(LTimer *t) {
    if (!t) return false;
    return t->mStarted;
}

bool LTimer_isPaused(LTimer *t) {
    if (!t) return false;
    return t->mPaused && t->mStarted;
}
