#include "Render.h"
#include "Polygon.h"
#include <assert.h>

void clearScreen(SDL_Renderer *globalRenderer, int r, int g, int b, int a) {
    SDL_SetRenderDrawColor(globalRenderer,r,g,b,a);
    SDL_RenderClear(globalRenderer);
}

void drawEntity( Entity *p ) {
    assert( p );
    double myscale = 2.0;
    int flipmode = !p->isFlipped ? SDL_FLIP_NONE : SDL_FLIP_HORIZONTAL;
    int result = Entity_draw(p, myscale, p->angle, NULL, flipmode);
    if (result) {
        mPrint("Error drawing player");
        exit(-1);
    }
}

void drawEntityAngle( Entity *p, double angle ) {
    assert( p );
    double myscale = 2.0;
    int flipmode = !p->isFlipped ? SDL_FLIP_NONE : SDL_FLIP_HORIZONTAL;
    int result = Entity_draw(p, myscale, angle, NULL, flipmode);
    if (result) {
        mPrint("Error drawing player");
        exit(-1);
    }

}



void drawObjects( Gamestate *gs) {
    //mPrint("drawObjects");
    if (!gs) return;
    drawEntity(gs->player);

    // draw enemies
    Entity *currentEntity = gs->enemyList;
    while (currentEntity != NULL) {
        double x = currentEntity->x;
        double y = currentEntity->y;
        if (currentEntity->isInnertube) {
            renderSquare(gs, (int)x, (int)y, 5, 5, 255, 255, 0, 255);
        }
        else {

            drawEntity(currentEntity);
        }
        currentEntity = currentEntity->next;
    }

    // draw bullets
    //mPrint("drawObjects");
    currentEntity = gs->bulletList;

    SDL_Rect rect = { -1, -1, -1, -1 };
    while (currentEntity != NULL) {
        //drawEntity(currentEntity);
        double angle = rand();
        drawEntityAngle(currentEntity, angle );
 
        // render a blue rectangle around the sprite area
    /*
        rect.x = (int)currentEntity -> x;
        rect.y = (int)currentEntity -> y;
        rect.w = currentEntity -> currentClip -> w * 2;
        rect.h = currentEntity -> currentClip -> h * 2;
        SDL_SetRenderDrawColor( gs->r, 0, 0, 255, 255 );
        SDL_RenderDrawRect( gs->r, &rect );
    */

        currentEntity = currentEntity->next;

    }

    //mPrint("drawObjects");
    // draw a white line
    // lock it to the skull
    if (gs->spacebarIsPressed) {
        double x0 = gs->DefaultScreenWidth/2;
        double y0 = gs->DefaultScreenHeight/2;
        double x1 = gs->player->x;
        double y1 = gs->player->y;
        drawLine( gs , (int)x0, (int)y0, (int)x1, (int)y1, 255, 255, 255);
    }
}





void updateClips( Gamestate *gs  ) {
    if (!gs) return;
    if (gs->frame_count % 8 == 0) 
        Entity_updateClip(gs->player);
}

void renderSquare( Gamestate *gamestate, int x,int y,int w,int h,int r,int g,int b,int a){
    SDL_Rect rect={x,y,w,h};
    SDL_SetRenderDrawColor(gamestate->r, r, g, b, a);
    SDL_RenderFillRect(gamestate->r, &rect);
}

void renderGrid(Gamestate *gs, int r,int g,int b,int a){
    SDL_SetRenderDrawColor(gs->r,r,g,b,a);
    int th = gs->tileSizeH; 
    int tw = gs->tileSizeW;
    int sc = (int) gs->scale * 2;
    int y_incr = th * sc;
    int x_incr = tw * sc;
    int width = gs->DefaultScreenWidth;
    int height = gs->DefaultScreenHeight;
    for (int y=0; y < height; y += y_incr )
        SDL_RenderDrawLine(gs->r, 0, y, width,y);
    for (int x=0; x < width; x += x_incr)
        SDL_RenderDrawLine(gs->r,x,0,x,height);
}

void handleRenderGrid( Gamestate *gs ) { 
    if (!gs) return;
    if (gs->renderGridOn) renderGrid( gs, 0x33,0x33,0x33,255); 
}

void handleRenderDebugPanel( Gamestate* gs ) { 
    if (!gs) return;
    if (gs->renderDebugPanelOn) renderDebugPanel( gs ); 
}

void renderDebugPanel( Gamestate *gs ) {
    if (!gs) return;
    SDL_Color color = {255,255,255,255};
    SDL_SetRenderDrawColor(gs->r,0,0,0,125); // black box
    SDL_RenderFillRect(gs->r, gs->debugPanelRect);
    
    SDL_SetRenderDrawColor(gs->r,0,0,255,255); // blue border
    SDL_RenderDrawRect(gs->r, gs->debugPanelRect);

    LTexture_loadFromRenderedText(gs->gFPSTexture, gs->debugPanelText, color);
    LTexture_render(gs->gFPSTexture, gs->debugPanelX, gs->debugPanelY, 1, NULL, 0, NULL, SDL_FLIP_NONE);
}



void renderPolygon( Gamestate *gs, Polygon *p ) {
    SDL_SetRenderDrawColor( gs->r, p->color.r, p->color.g, p->color.b, p->color.a );
    for (unsigned int i = 0; i < p->sides-1; i++) {

        SDL_RenderDrawLine( gs->r, 
                    p->points[i].x, 
                    p->points[i].y, 
                    p->points[i+1].x, 
                    p->points[i+1].y );
        }

        SDL_RenderDrawLine( gs->r, 
                p->points[ p->sides-1 ].x, 
                p->points[ p->sides-1 ].y, 
                p->points[ 0 ].x, 
                p->points[ 0 ].y );

}


//draw one quadrant arc, and mirror the other 4 quadrants
//source: https://stackoverflow.com/questions/38334081/howto-draw-circles-arcs-and-vector-graphics-in-sdl
void drawEllipse(SDL_Renderer* r, int x0, int y0, int radiusX, int radiusY) {
    assert(r);
    assert(radiusX > 0);
    assert(radiusY > 0);
    
    float pi  = (float)3.14159265358979323846264338327950288419716939937510;
    
    float pih = (float)(pi / 2.0); //half of pi

    //drew  28 lines with   4x4  circle with precision of 150 0ms
    //drew 132 lines with  25x14 circle with precision of 150 0ms
    //drew 152 lines with 100x50 circle with precision of 150 3ms
    const int prec = 27; // precision value; value of 1 will draw a diamond, 27 makes pretty smooth circles.
    //float theta = 0;     // angle that will be increased each loop
    float theta = 0;     // angle that will be increased each loop

    //starting point
    int x  = (int)(radiusX * cos(theta));//start point
    int y  = (int)(radiusY * sin(theta));//start point
    int x1 = x;
    int y1 = y;

    //repeat until theta >= 90;
    float step = pih/(float)prec; // amount to add to theta each time (degrees)
    
    for(theta=step;  theta <= pih;  theta+=step) {
    //step through only a 90 arc (1 quadrant)
        //get new point location
        x1 = (int)((radiusX * cosf(theta)) + 0.5); //new point (+.5 is a quick rounding method)
        y1 = (int)((radiusY * sinf(theta)) + 0.5); //new point (+.5 is a quick rounding method)

        //draw line from previous point to new point, ONLY if point incremented
        if( (x != x1) || (y != y1) ) {
            //only draw if coordinate changed
            SDL_RenderDrawLine(r, x0 + x, y0 - y,    x0 + x1, y0 - y1 );//quadrant TR
            SDL_RenderDrawLine(r, x0 - x, y0 - y,    x0 - x1, y0 - y1 );//quadrant TL
            SDL_RenderDrawLine(r, x0 - x, y0 + y,    x0 - x1, y0 + y1 );//quadrant BL
            SDL_RenderDrawLine(r, x0 + x, y0 + y,    x0 + x1, y0 + y1 );//quadrant BR
        }
        //save previous points
        x = x1;//save new previous point
        y = y1;//save new previous point
    }

    //arc did not finish because of rounding, so finish the arc
    if(x!=0) {
        x=0;
        SDL_RenderDrawLine(r, x0 + x, y0 - y,    x0 + x1, y0 - y1 );//quadrant TR
        SDL_RenderDrawLine(r, x0 - x, y0 - y,    x0 - x1, y0 - y1 );//quadrant TL
        SDL_RenderDrawLine(r, x0 - x, y0 + y,    x0 - x1, y0 + y1 );//quadrant BL
        SDL_RenderDrawLine(r, x0 + x, y0 + y,    x0 + x1, y0 + y1 );//quadrant BR
    }
}


void drawCircle(SDL_Renderer* r, int x0, int y0, int radius) {
    drawEllipse(r, x0, y0, radius, radius);
}


void drawTitleScreen(Gamestate *gs) {
 
    assert( gs );
    //drawEntity( gs->titleScreenEntity );

    Entity *p = gs->titleScreenEntity;

    double myscale = 1.0;
    int flipmode = !p->isFlipped ? SDL_FLIP_NONE : SDL_FLIP_HORIZONTAL;
    int result = Entity_draw(p, myscale, p->angle, NULL, flipmode);
    if (result) {
        mPrint("Error drawing titlescreen");
        exit(-1);
    }


    SDL_SetRenderDrawColor( gs->r, 0, 0, 255, 255 );
    SDL_Rect rect = { -1, -1, -1, -1 };
    rect.x = (int) p->x;
    rect.y = (int) p->y;
    rect.w = p->currentClip->w;
    rect.h = p->currentClip->h;
    SDL_RenderDrawRect( gs->r, &rect );
}


void renderFrame( Gamestate *gs ) {    
    
    assert (gs);

    if ( !gs ) return;

    // in all scenes, clear the screen
    clearScreen(gs->r, 255,0,0,255);
    
    // in all scenes, we do a targetTexture change so we can draw to a surface
    // and scale it later
    LTexture_setAsRenderTarget(gs->targetTexture);
    
    // clear the targetTexture
    clearScreen(gs->r, 0,0,0,255);

    // this is the main game scene drawing

    if ( gs->currentScene == GamestateSceneMainGame ) {
        drawTubeOuter(gs);
        drawTubeInner(gs);
        drawObjects(gs);
        updateClips(gs);
        handleRenderGrid(gs);
    }
    else if ( gs->currentScene == GamestateSceneTitle ) {
        drawTitleScreen(gs);
    }
    //else if ( gs->currentScene == GamestateSceneNone ) {
    //}

    //in all scenes, reset render target
    SDL_SetRenderTarget(gs->r, NULL);
    
    // in all scenes, draw targetTexture
    LTexture_render(gs->targetTexture, gs->camera_x, gs->camera_y, gs->scale, NULL, 0.0, NULL, SDL_FLIP_NONE );
    
    // in all scenes, handle debug panel
    handleRenderDebugPanel(gs);

    // in all scenes, present renderer and increment framecount
    SDL_RenderPresent(gs->r);
    gs->frame_count++;   
}


void drawTubeOuter(Gamestate *gs) {
    SDL_SetRenderDrawColor(gs->r, 255, 0, 0, 255);
    int x = gs->DefaultScreenWidth/2;
    int y = gs->DefaultScreenHeight/2;
    int radius = gs->circleRadius;
    drawCircle(gs->r, x, y, radius );
}


void drawTubeInner(Gamestate *gs) {
    SDL_SetRenderDrawColor(gs->r, 255, 0, 0, 255);
    int x = gs->innertubeRect->x;
    int y = gs->innertubeRect->y;
    int w = gs->innertubeRect->w;
    int h = gs->innertubeRect->h;
    

    SDL_Rect rect = { x, y, w, h };
    SDL_RenderDrawRect(gs->r, &rect );
}


void drawLine(Gamestate *gs, int x0, int y0, int x1, int y1, int r, int g, int b) {
    SDL_SetRenderDrawColor(gs->r, r, g, b, 255);
    SDL_RenderDrawLine(gs->r, (int)x0, (int)y0, (int)x1, (int)y1);
}






