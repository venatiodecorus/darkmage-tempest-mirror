//#pragma once 

#ifndef __GAMESTATE_H
#define __GAMESTATE_H

#include "LTexture.h"
#include "SDL_handler.h"
#include "Entity.h"
#include "mPrint.h"

#include <stdlib.h>
#include <string.h>

#include <SDL_mixer.h>

#define TILE_SIZE_W_DEFAULT 24
#define TILE_SIZE_H_DEFAULT 24


typedef enum {
    GamestateSceneNone = 0,

    GamestateSceneTitle,
    GamestateSceneMainGame,
    GamestateSceneCount

} GamestateScene;



typedef enum {
    MOVEMODE_CAMERA = 0,
    MOVEMODE_PLAYER,
    MOVEMODE_COUNT
} MoveMode_t;





typedef struct {

    
    MoveMode_t moveMode;


    GamestateScene currentScene;


    int DefaultScreenWidth;
    int DefaultScreenHeight; // 720p at half-size
    int camera_x;
    int camera_y;
    int tileSizeW;
    int tileSizeH;
    
    double scale;
    
    LTexture *targetTexture;

    SDL_Renderer *r;
 
    Entity *titleScreenEntity;

    Entity *player;
    
    Entity *enemyList;
    Entity *bulletList;
    int enemyCount;
    int bulletCount;
    
    bool spacebarIsPressed; // handling shots
    bool leftIsPressed;
    bool rightIsPressed;
    bool upIsPressed;
    bool downIsPressed;
    bool fireIsPressed;

    char debugPanelText[1024];
    
    bool renderDebugPanelOn;
    bool renderGridOn;
    
    SDL_Rect *debugPanelRect;
    SDL_Rect *innertubeRect;
    
    LTexture *gFPSTexture;
    
    int debugPanelX;
    int debugPanelY;
    int debugPanelWidth;
    int debugPanelHeight;
    int frame_count;

    int mouse_x;
    int mouse_y;
    int rel_mouse_x;
    int rel_mouse_y;

    int circleRadius;

    double playerAngle; // angle around center of circle
    double avgFPS;


    int enemiesKilled;



    Mix_Music *backgroundMusic;
    Mix_Chunk *splatEffect;



} Gamestate;

Gamestate *Gamestate_new ( int cx, int cy, double sc );
void       Gamestate_free( Gamestate *gs );


void handleEntityMove( Gamestate *g, Entity *p ) ;
void handlePlayerMove( Gamestate *g, Entity *p ) ;
void handleEnemyMove( Gamestate *g ) ;
void handleBulletMove( Gamestate *gamestate ) ;

void handleEntityAnimation( Entity *p );

void updateState(Gamestate *gs);

void Gamestate_setCircleRadius(Gamestate *gs, int r) ;
void Gamestate_setPlayerAngle(Gamestate *gs, double angle) ;

bool pixelCollides(int x, int y, SDL_Rect *rect) ;

void updateEnemyList(Gamestate *gamestate) ;
void addBullet( Gamestate *gs ) ;
double distance(double x0, double y0, double x1, double y1) ;

#endif

