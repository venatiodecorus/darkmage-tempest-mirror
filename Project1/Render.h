#pragma once 

#include "SDL_handler.h"
#include "Gamestate.h"
#include "Entity.h"
#include "LTexture.h"

void clearScreen(SDL_Renderer *globalRenderer, int r, int g, int b, int a);
void drawObjects( Gamestate *gs) ;

void drawEntity( Entity *p ) ;
void drawEntityAngle( Entity *p, double angle ) ;

void updateClips( Gamestate *gs  ) ;
void renderSquare( Gamestate *gamestate, int x,int y,int w,int h,int r,int g,int b,int a);
void renderGrid(Gamestate *gs, int r,int g,int b,int a);
void handleRenderGrid( Gamestate *gs ); 
void handleRenderDebugPanel( Gamestate* gs ) ;
void renderDebugPanel( Gamestate *gs ) ;


void renderFrame( Gamestate *gs );
void drawEllipse(SDL_Renderer* r, int x0, int y0, int radiusX, int radiusY) ;
void drawCircle(SDL_Renderer* r, int x0, int y0, int radius);
void drawTubeInner(Gamestate *gs);
void drawTubeOuter(Gamestate *gs);

void drawLine(Gamestate *gs, int x0, int y0, int x1, int y1, int r, int g, int b);
//void drawLine(Gamestate *gs, double x0, double y0, double x1, double y1, double r, double g, double b) ;

void drawTriangle(Gamestate *gs);
//void renderPolygon( Gamestate *gs, Polygon *p );


void drawTitleScreen(Gamestate *gs) ;
