#pragma once
#include "SDL_handler.h"
#include <stdbool.h>

typedef struct {
    SDL_Texture *mTexture;
    SDL_Renderer *gRenderer;
    TTF_Font *gFont;
    int mWidth;
    int mHeight;

    void *mPixels;
    int mPitch;

} LTexture;

LTexture *LTexture_new() ;

bool LTexture_create(LTexture *t, SDL_Renderer *r, Uint32 format, int access, int w, int h);
bool LTexture_loadFromFile( LTexture *t, char *path);

//Creates image from font string
bool LTexture_loadFromRenderedText(LTexture *t, char *textureText, SDL_Color textColor);

//Deallocates texture
void LTexture_free(LTexture *t);

//Renders texture at given point
void LTexture_render(LTexture *t, int x, int y, double scale, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip);

//Set color modulation
void LTexture_setColor(LTexture *t,Uint8 r,Uint8 g,Uint8 b);

void LTexture_setBlendMode(LTexture *t,SDL_BlendMode blending);

void LTexture_setAlpha(LTexture *t, Uint8 alpha);

int LTexture_getWidth(LTexture *t);

int LTexture_getHeight(LTexture *t);

void LTexture_setRenderer(LTexture *t, SDL_Renderer *r);

void LTexture_setFont(LTexture *t,TTF_Font *font);

SDL_Renderer *LTexture_getRenderer(LTexture *t);

TTF_Font *LTexture_getFont(LTexture *t);

// new as of 7/10/2020
void LTexture_setAsRenderTarget(LTexture *t);

// new as of 10/8/2020
Uint32 LTexture_getPixel32( LTexture *t, unsigned int x, unsigned int y ); 

bool   LTexture_lockTexture(LTexture *t);
bool   LTexture_unlockTexture(LTexture *t);
void * LTexture_getPixels(LTexture *t);
int    LTexture_getPitch(LTexture *t);
