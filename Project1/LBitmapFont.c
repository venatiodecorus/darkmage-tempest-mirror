#include "LBitmapFont.h"
#include "mPrint.h"
#include <stdio.h>
#include <string.h>

LBitmapFont * LBitmapFont_createNew() {
    LBitmapFont *font = (LBitmapFont *) malloc( sizeof(LBitmapFont) );

    if (!font) {
        mPrint("Error creating new LBitmapFont");
        exit(-1);
    }

    font->mBitmap = NULL;
    font->mNewline = 0;
    font->mSpace = 0;

    return font;
}

bool LBitmapFont_buildFont( LBitmapFont *font, LTexture *bitmap ) {
    mPrint("Begin buildFont");

    if (!font) {
        mPrint("Error building LBitmapFont");
        //exit(-1);
        return false;
    }

    if (!bitmap) {
        mPrint("Error building LBitmapFont");
        //exit(-1);
        return false;
    }

    if (! LTexture_lockTexture( bitmap ) ) {
        mPrint("Unable to lock texture");
        return false;
    }

    mPrint("Initial checks passed");

    Uint32 bgColor = LTexture_getPixel32( bitmap, 0, 0 );
    int cellW = LTexture_getWidth( bitmap ); 
    int cellH = LTexture_getHeight( bitmap ); 

    int top = cellH;
    int baseA = cellH;

    int currentChar = 0;

    mPrint("Entering loop");
    //rows
    for (int i = 0; i < 16; i++) {
        //cols
        for (int j = 0; j < 16; j++ ) {

            printf("currentChar: %u\n", currentChar);

            font->mChars[ currentChar ].x = cellW * j;
            font->mChars[ currentChar ].y = cellH * i;
            font->mChars[ currentChar ].w = cellW;
            font->mChars[ currentChar ].h = cellH;

            mPrint("inner loop 1");
            printf("i: %d\n", i);
            printf("j: %d\n", j);
            for (int pCol = 0; pCol < cellW; pCol++) {
                for (int pRow = 0; pRow < cellH; pRow++) {
                    int pX = (cellW * j) + pCol;
                    int pY = (cellH * i) + pRow;

                    printf("pX: %d\n", pX);
                    printf("pY: %d\n", pY);
                    if ( LTexture_getPixel32( bitmap, pX, pY ) != bgColor ) {
                        
                        font->mChars[ currentChar ].x = pX;

                        pCol = cellW;
                        pRow = cellH;
                    }
                }
            }


            mPrint("inner loop 2");
			for( int pColW = cellW - 1; pColW >= 0; pColW-- ) {
				//Go through pixel rows
				for( int pRowW = 0; pRowW < cellH; pRowW++ ) {
					//Get the pixel offsets
					int pX = ( cellW * j ) + pColW;
					int pY = ( cellH * i ) + pRowW;

					//If a non colorkey pixel is found
					if( LTexture_getPixel32( bitmap, pX, pY ) != bgColor ) {
						//Set the width
						font->mChars[ currentChar ].w = ( pX - font->mChars[ currentChar ].x ) + 1;

						//Break the loops
						pColW = -1;
						pRowW = cellH;
					}
				}
			}

            //Find Top
            //Go through pixel rows
            mPrint("inner loop 3");
            for( int pRow = 0; pRow < cellH; ++pRow ) {
                //Go through pixel columns
                for( int pCol = 0; pCol < cellW; ++pCol ) {
                    //Get the pixel offsets
                    int pX = ( cellW * j ) + pCol;
                    int pY = ( cellH * i ) + pRow;

                    //If a non colorkey pixel is found
                    if( LTexture_getPixel32( bitmap, pX, pY ) != bgColor ) {
                        //If new top is found
                        if( pRow < top ) {
                            top = pRow;
                        }

                        //Break the loops
                        pCol = cellW;
                        pRow = cellH;
                    }
                }
            }


            //Find Bottom of A
            mPrint("If currentChar is A");
            if( currentChar == 'A' ) {
                //Go through pixel rows
                mPrint("inner loop 4");
                for( int pRow = cellH - 1; pRow >= 0; --pRow ) {
                    //Go through pixel columns
                    for( int pCol = 0; pCol < cellW; ++pCol ) {
                        //Get the pixel offsets
                        int pX = ( cellW * j ) + pCol;
                        int pY = ( cellH * i ) + pRow;
                        //If a non colorkey pixel is found
                        if( LTexture_getPixel32( bitmap, pX, pY ) != bgColor ) {
                            //Bottom of a is found
                            baseA = pRow;
                            //Break the loops
                            pCol = cellW;
                            pRow = -1;
                        }
                    }
                }
            }

            currentChar++;
        }
    }
    mPrint("Exitted loop");

    font->mSpace = cellW / 2;
    font->mNewline = baseA - top;
    for (int i = 0; i < 256; i++) {
        font->mChars[ i ].y += top;
        font->mChars[ i ].h -= top;
    }

    LTexture_unlockTexture( bitmap );

    font->mBitmap = bitmap;

    mPrint("End buildFont");
    return true;
}







void LBitmapFont_renderText( LBitmapFont *font, int x, int y, char *text ) {

    if ( !font ) return ;
    if ( !font->mBitmap ) return;

    int curX = x;
    int curY = y;

    for (unsigned int i = 0; i < strlen(text); i++) {
        
        if (text[i]==' ') {
            curX += font->mSpace;
        }
        else if (text[i]=='\n') {
            curY += font->mNewline;
            curX = x;
        }
        else {
            int ascii = (unsigned char) text[i];

            LTexture_render( font->mBitmap, curX, curY, 1.0, &font->mChars[ascii], 0, NULL, SDL_FLIP_NONE );

            curX += font->mChars[ascii].w + 1;
        }
    }
}















