# TODO

- Enemy generation
    - drawing the enemies inside the box of the inner tube
    - drawing the enemies between the tubes 
- Collision detection
- Bullet / dagger
- Re-implement mouse-support anchoring the skull to the mouse

- *optional*: Multi-threading for either:
    - rendering frames
    - receiving user input

# DONE

- Undo mouse-support temporarily
- Fullscreen 
- Laser 
- Fix glitch between releasing spacebar and holding left or right
