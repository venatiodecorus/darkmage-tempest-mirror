#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "LTexture.h"
#include "mPrint.h"

typedef enum {

    EntityType_None = 0,
    
    EntityType_Player,
    
    EntityType_Enemy,

    EntityType_Bullet,
    
    EntityType_Titlescreen,

} EntityType;



typedef struct {
    double x;
    double y;
    int x_dest;
    int y_dest;
    double xv;
    double yv;

    int clipIndex;
    int numClips;
    int animContext;
    int animContextCount;
    SDL_Rect *currentClip;
    LTexture *spritesheet;
    SDL_Rect *currentSpritesheetRect;
    bool isFlipped;
    bool isAnimating;

    double angle;

    bool isPlayer;
    bool isEnemy;

    EntityType entityType;

    bool isVisible;
    bool isInnertube;

    bool isDestroyed;

    void *next;
    void *prev;

} Entity;

Entity *newEntity(int x, int y, int w, int h) ;

void Entity_free(Entity *p);

int Entity_loadSpritesheet(Entity *p, char *filename, int numClips, SDL_Renderer *r); 

int Entity_draw(Entity *p, double scale, double angle, SDL_Point *center, SDL_RendererFlip flip) ;

void Entity_updateClip(Entity *p) ;

void Entity_flipAnimationOnOff(Entity* p);
