#include "Polygon.h"

#include <stdio.h>
#include <assert.h>

#include "Render.h"
#include "mPrint.h"


Polygon *Polygon_new() {
    Polygon *p = (Polygon *) calloc(1, sizeof(Polygon));
    if (!p) {
        mPrint("Error allocating Polygon, exiting");
        exit(-1);
    }
    p->sides = -1;
    p->points = NULL;
    p->color.r = 255;
    p->color.g = 0;
    p->color.b = 0;
    p->color.a = 255;
    return p;
}

Polygon *Polygon_new_with_sides(int s) {
    if (s <= 2) {
        mPrint("Error creating new Polygon, sides is less than or equal to 2");
        return NULL;
    }
    Polygon *p = Polygon_new();
    p->sides = s;
    p->points = (SDL_Point *) calloc( sizeof(SDL_Point), s );
    if (p->points == NULL) {
        mPrint("Failed to allocate points in new polygon");
        return NULL;
    }
    // all points are zero'd out atm, up to caller to assign point values
    return p;
}


void Polygon_setPointXY(Polygon *p, unsigned int index, int x, int y) {
    if (!p) return;
    if (index < 0) {
        mPrint("index is < 0");
        return;
    }
    else if (index >= p->sides) {
        mPrint("index is >= sides");
        return;
    }
    p->points[ index ].x = x;
    p->points[ index ].y = y;
}


void Polygon_free(Polygon *p) {
    if (!p) return;
    free(p->points);
    p->points = NULL;
    free(p);
}


SDL_Point * Polygon_getPointXY(Polygon *p, unsigned int index) {
    if (!p) {
        return NULL;
    }
    if ( index < 0 || index >= p->sides ) {
        return NULL;
    }
    SDL_Point *point = &(p->points[ index ]);
    return point;
}


void Polygon_setColorRGBA( Polygon *p, int r, int g, int b, int a ) {
    assert(p);
    p->color.r = r;
    p->color.g = g;
    p->color.b = b;
    p->color.a = a;
}


