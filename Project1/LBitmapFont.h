
#include "SDL_handler.h"
#include "LTexture.h"

typedef struct {
    LTexture *mBitmap;
    SDL_Rect mChars[ 256 ];
    int mNewline;
    int mSpace;
} LBitmapFont;

LBitmapFont * LBitmapFont_createNew();
bool LBitmapFont_buildFont( LBitmapFont *font, LTexture *bitmap );
void LBitmapFont_renderText( LBitmapFont *font, int x, int y, char *text );

